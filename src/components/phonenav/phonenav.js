import PhoneSideBar from '../PhoneSideBar/PhoneSideBar'
import { serverBus } from '../../main.js'
export default {
  component:{
    PhoneSideBar,
  },
    mounted() {
      this.menuItems = document.querySelectorAll('.MegaMenu a')
    },
    data() {
      return {
        isVisible: false,
        menuItems: null,
        focusedIndex: 0,
        isopen:0,
        modalOpen:false
      }
    },
    methods: {
      openmodal(){
        serverBus.$emit('serverSelected', !this.modalOpen)
        serverBus.$emit('serverSelected1',this.isopen+1)
      },
      hideMenu() {
        this.isVisible = false
        this.focusedIndex = 0
      },
      startArrowKeys() {
        this.menuItems[0].focus()
      },
      focusPrevious(isArrowKey) {
        this.focusedIndex = this.focusedIndex - 1

        if (isArrowKey) {
          this.focusItem()
        }
      },
      focusNext(isArrowKey) {
        this.focusedIndex = this.focusedIndex + 1

        if (isArrowKey) {
          this.focusItem()
        }
      },
      focusItem() {
        this.menuItems[this.focusedIndex].focus()
      }
    }
  }
