const now = new Date();
const year = now.getFullYear();
export default {
  data() {
    return {
      msg: 'Welcome to Your Vue.js App',
      year,
      slide: 0,
      sliding: null,
      img1:'product1.png',
      img2:'product1.png',
      img3:'product1.png',
      img4:'product1.png',
      img5:'product1.png',
      img6:'product1.png',
      img7:'product1.png',
      img8:'product1.png',
      img9:'product1.png',
      expectwork:[
        {
          csslevel1:'product11 productboxitem',
          name:'productType1',
          index:1,
        animationdelaytime:300,
          img:'product1.png',
          nonactive:'product1.png',
          active:'hoverproduct1.png'
        },
        {
          csslevel1:'product22 productboxitem',
          name:'productType2',
          index:2,
          animationdelaytime:600,
          img:'product2.png',
          nonactive:'product2.png',
          active:'hoverproduct2.png'
        },
        {
          csslevel1:'product33 productboxitem',
          name:'productType3',
          index:3,
          animationdelaytime:900,
          img:'product3.png',
          nonactive:'product3.png',
          active:'hoverproduct3.png'
        },
        {
          csslevel1:'product44 productboxitem',
          name:'productType4',
          index:4,
          animationdelaytime:300,
          img:'product4.png',
          nonactive:'product4.png',
          active:'hoverproduct4.png'
    
        },
        {
          csslevel1:'product55 productboxitem',
          name:'productType5',
          index:5,
          animationdelaytime:300,
          img:'product5.png',
          nonactive:'product5.png',
          active:'hoverproduct5.png'
    
        },
        {
          csslevel1:'product66 productboxitem',
          name:'productType6',
          index:6,
          animationdelaytime:300,    
          img:'product6.png',
          nonactive:'product6.png',
          active:'hoverproduct6.png'
    
        },
        {
          csslevel1:'product77 productboxitem',
          name:'productType7',
          index:7,
          animationdelaytime:300,
          img:'product7.png',
          nonactive:'product7.png',
          active:'hoverproduct7.png'
    
        },
        {
          csslevel1:'product88 productboxitem',
          name:'productType8',
          index:8,
          animationdelaytime:300,
          img:'product8.png',
          nonactive:'product8.png',
          active:'hoverproduct8.png'
    
        },
        {
          csslevel1:'product99 productboxitem',
          name:'productType9',
          index:9,
          animationdelaytime:300,
          img:'product9.png',
          nonactive:'product9.png',
          active:'hoverproduct9.png'
    
        }
      ],
    }
  },
  methods: {
    onSlideStart(slide) {
      this.sliding = true
    },
    onSlideEnd(slide) {
      this.sliding = false
    },
    gotowork(item) {
      this.$router.push('/work/' + item)
    },
  }
}
