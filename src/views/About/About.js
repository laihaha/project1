import MegaMenu from '@/components/WebNav/WebNav.vue'
import PhoneMenu from '@/components/PhoneNav/PhoneNav.vue'
import TopBanner from '@/components/TopBanner/TopBanner.vue'
import WebAboutContent from '@/components/WebAboutContent/WebAboutContent.vue'
import PhoneAboutContent from '@/components/PhoneAboutContent/PhoneAboutContent.vue'
import SubFooter from '@/components/SubFooter/SubFooter.vue'
import WebMenuBar from '@/components/WebMenuBar/WebMenuBar.vue'
import PhoneSideBar from '@/components/PhoneSideBar/PhoneSideBar.vue'
import baseServ from '@/share/base.service'
import ModalPlugin from 'bootstrap-vue'
import WebFooter from '@/components/WebFooter/WebFooter.vue'
import PhoneFooter from '@/components/PhoneFooter/PhoneFooter.vue'
import { serverBus } from '../../main.js';
export default {
  name: 'About',
  components: {
    MegaMenu,
    PhoneMenu,
    TopBanner,
    WebAboutContent,
    PhoneAboutContent,
    SubFooter,
    WebMenuBar,
    PhoneSideBar,
    baseServ,
    ModalPlugin,
    WebFooter,
    PhoneFooter
  },
  data() {
    return {
      msg: 'Welcome to Your Vue.js App',
      device: baseServ.Device,
      modalOpen:false,
      isModalVisible: false,
      isopen:1,
      scrolledToBottom: false,
      page_title: 'About Us',
      items: [{
        text: 'HOME',
        href: '/home'
      }, {
        text: 'ABOUT US',
        active: true
      }],
    }
  },
  methods: {
    scroll() {
      window.onscroll = () => {
        let bottomOfWindow = Math.max(window.pageYOffset, document.documentElement.scrollTop, document.body.scrollTop) + window.innerHeight === document.documentElement.offsetHeight

        if (bottomOfWindow) {
          this.scrolledToBottom = true // replace it with your code
        }
      }
    },
    scrollToTop() {
      window.scrollTo(0, 0);
    }
  },
  mounted() {
    this.scroll()
    serverBus.$on('serverSelected', (server) => {
      this.modalOpen = server;
  });
  serverBus.$on('serverSelected1', (server1) => {
    this.isopen = server1;
   });
  },
  created() {
    window.scrollTo(0,0);
    // Using the server bus
    serverBus.$on('serverSelected', (server) => {
        this.modalOpen = server;
    });
    serverBus.$on('serverSelected1', (server1) => {
      this.isopen = server1;
     });
   }
}
