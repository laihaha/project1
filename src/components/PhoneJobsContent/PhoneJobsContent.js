const now = new Date();
const year = now.getFullYear();
export default {
  data() {
    return {
      msg: 'Welcome to Your Vue.js App',
      year,
      selected: [], // Must be an array reference!
      options: [
      {
        text: 'Mr',
        value: 'M'
      },
      {
        text: 'Ms',
        value: 'F'
      }
      ],
      items: [
      {
        text: 'HOME',
        href: '/home'
      }, {
        text: 'OUR MISSION',
        href: '/Mission'
      }
     ],  
    }
  }
}
