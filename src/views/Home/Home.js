import MegaMenu from '@/components/WebNav/WebNav.vue'
import PhoneMenu from '@/components/PhoneNav/PhoneNav.vue'
import WebHomeContent from '@/components/WebHomeContent/WebHomeContent.vue'
import PhoneHomeContent from '@/components/PhoneHomeContent/PhoneHomeContent.vue'
import WebMenuBar from '@/components/WebMenuBar/WebMenuBar.vue'
import SubFooter from '@/components/SubFooter/SubFooter.vue'
import WebFooter from '@/components/WebFooter/WebFooter.vue'
import PhoneFooter from '@/components/PhoneFooter/PhoneFooter.vue'
import PhoneSideBar from '@/components/PhoneSideBar/PhoneSideBar.vue'
import modal from '@/components/ContactUsModal/ContactUsModal.vue'
import baseServ from '@/share/base.service'
import ModalPlugin from 'bootstrap-vue'
import { serverBus } from '../../main.js';
export default {
  name: 'Home',
  components: {
    MegaMenu,
    PhoneMenu,
    SubFooter,
    PhoneHomeContent,
    WebHomeContent,
    WebMenuBar,
    PhoneSideBar,
    baseServ,
    ModalPlugin,
    modal,
    WebFooter,
    PhoneFooter
  },
  data () {
    return {
      msg: 'Welcome to Your Vue.js App',
      device: baseServ.Device,
      modalOpen:false,
      isModalVisible: false,
      isopen:1,
      scrolledToBottom:false
    }
  },
  methods: {
    scroll () {
      window.onscroll = () => {
        let bottomOfWindow = Math.max(window.pageYOffset, document.documentElement.scrollTop, document.body.scrollTop) + window.innerHeight === document.documentElement.offsetHeight

        if (bottomOfWindow) {
         this.scrolledToBottom = true // replace it with your code
        }
      }
     },
     scrollToTop() {
      window.scrollTo(0,0);
    },
    showModal1() {
      this.isModalVisible = true;
    },
    closeModal() {
      this.isModalVisible = false;
    }
  },
    mounted () {
      this.scroll()
    },
  created() {
    // Using the server bus
    serverBus.$on('serverSelected', (server) => {
     this.modalOpen = server;
    });
    serverBus.$on('serverSelected1', (server1) => {
      this.isopen = server1;
     });
     serverBus.$on('serverSelected2', (server2) => {
      this.isModalVisible = server2;
     });
   }
}
