import { serverBus } from '../../main.js'
export default {
  name: 'modal',
  data () {
    return {
      isVisible: false,
      menuItems: null,
      focusedIndex: 0,
      modalOpen: false,
      select: 0,
      isopen: 0,
      menubar: [
        {
          name: 'home',
          link: '/home',
          css1: 'one1',
          index: 1
        },
        {
          name: 'about',
          link: '/about',
          css1: 'one2',
          index: 2
        },
        {
          name: 'work',
          link: '/work',
          css1: 'one3',
          index: 3
        },
        {
          name: 'jobs',
          link: '/jobs',
          css1: 'one4',
          index: 4
        },
        {
          name: 'contact',
          link: '/contact',
          css1: 'one5',
          index: 5
        }
      ]
    }
  },
  methods: {
    close () {
      serverBus.$emit('serverSelected', !this.modalOpen)
      serverBus.$emit('serverSelected1', this.isopen = 0)
    },
    mouseleave1 () {
      this.select = 0
    },
    mouseover1 (item) {
      this.select = item.index
    },
    goto (item) {
      if (item.index === 3) {
        this.select = item.index
      } else {
        this.$router.push(item.link)
        this.close()
      }
    },
    gotoWork (item) {
      this.$router.push('/work/' + item)
      this.close()
    },
    openlink (item) {
      this.$router.push(item)
      this.close()
    }
  },
  created () {
    serverBus.$on('serverSelected', (server) => {
      this.modalOpen = server
    })
    serverBus.$on('serverSelected1', (server1) => {
      this.isopen = server1
    })
  }
}
