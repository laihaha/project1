import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      redirect: '/home'
    },
    {
      path: '/home',
      name: 'Home',
      component: () => import('../views/Home/Home.vue')
    },
    {
      path: '/contact',
      name: 'Contact',
      component: () => import('../views/Contact/Contact.vue')
    },
    {
      path: '/about',
      name: 'About',
      component: () => import('../views/About/About.vue')
    },
    {
      path: '/mission',
      name: 'Mission',
      component: () => import('../views/Mission/Mission.vue')
    },
    {
      path: '/jobs/developer',
      name: 'Developer',
      component: () => import('../views/Developer/Developer.vue')
    },
    {
      path: '/jobs/designer',
      name: 'Designer',
      component: () => import('../views/Designer/Designer.vue')
    },
    {
      path: '/jobs/apply',
      name: 'Apply for Job',
      component: () => import('../views/ApplyJob/ApplyJob.vue')
    },
    {
      path: '/jobs',
      name: 'Jobs',
      component: () => import('../views/Jobs/Jobs.vue')
    },
    {
      path: '/work/:type',
      name: 'Work',
      component: () => import('../views/Work/Work.vue')
    }
  ]
})
