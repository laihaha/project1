import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import WebMenuBar from '@/components/WebMenuBar/WebMenuBar.vue'
import Language from '@/components/Language/Language.vue'
export default {
  name: 'app',
  components: {
    WebMenuBar,
    Language
  }
}
