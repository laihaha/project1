const now = new Date();
const year = now.getFullYear();
export default {
  data() {
    return {
      msg: 'Welcome to Your Vue.js App',
      year,
      type:null,
      selectitem:[],
      window: {
        width: 0,
        height: 0
      },
      cssWork:{
        part1height:0,
        part1height1:0,
        extrapadding:0,
        extrapadding1:0,
        col:'col-6',
      },
      categorywork:[
        {
          index:1,
          title:'productType1',
          imgcategory:'strategi_red.png',
          img:'phone1.png',
          css:'class1p col-11 relativep',
          description1:'productdes1',
          bgimg:[{
            bgimg:1,
            class:"col-3 no_pd_both fadeinimgp",
            data:'img1bg_v1.png'
          },
          {
            bgimg:2,
            class:"col-3 no_pd_both fadeinimgp",
            data:'img1bg_v2.png'
          },
          {
            bgimg:3,
            class:"col-3 no_pd_both fadeinimgp",
            data:'img1bg_v3.png'
          },
          {
            bgimg:4,
            class:"col-3 no_pd_both fadeinimgp",
            data:'img1bg_v4.png'
          }
        ],
          moreimg:[
            {
              imgno:1,
              class:"first img100 imgboxp animatef",
              data:'img1bg.png'
            },
            {
              imgno:2,
              class:'second img100 imgboxp animates',
              data:'img1item.png'
            },
            {
              imgno:3,
              class:'third img100 imgboxp animatel',
              data:'img1text.png'
            }
          ],
          paragraph:[
            {
              index1:1,
              data:'productparag1v1'
            },
            {
              index1:2,
              data:'productparag1v2'
            },
            {
              index1:3,
              data:'productparag1v3'
            },
            {
              index1:4,
              data:'productparag1v4'
            }
          ],
          paragraphmore:[
          ]
        },
        {
          index:2,
          title:'productType2',
          imgcategory:'branding_red.png',
          img:'phone2.png',
          css:'class1p col-11 relativep',
          bgimg:[{
            bgimg:1,
            class:"col-3 no_pd_both fadeinimgp",
            data:'img2bg_v1.png'
          },
          {
            bgimg:2,
            class:"col-3 no_pd_both fadeinimgp",
            data:'img2bg_v2.png'
          },
          {
            bgimg:3,
            class:"col-3 no_pd_both fadeinimgp",
            data:'img2bg_v3.png'
          },
          {
            bgimg:4,
            class:"col-3 no_pd_both fadeinimgp",
            data:'img2bg_v4.png'
          }
        ],
          moreimg:[
            {
              imgno:1,
              class:"first img100 imgboxp animatef",
              data:'img2bg.png'
            },
            {
              imgno:2,
              class:'second img100 imgboxp animates',
              data:'img2item.png'
            },
            {
              imgno:3,
              class:'third img100 imgboxp animatel ',
              data:'img2text.png'
            }
          ],
          description1:'productdes2',
          paragraph:[
            {
              index1:1,
              class:'normal',
              data:'productparag2v1'
            },
            {
              index1:2,
              class:'normal',
              data:'productparag2v2'
            },
            {
              index1:3,
              class:'normal',
              data:'productparag2v3'
            },
            {
              index1:4,
              class:'normal',
              data:'productparag2v4'
            }
          ],
          paragraphmore:[
          ]
        },
        {
          index:3,
          title:'productType3',
          imgcategory:'digitak_red.png',
          img:'phone3.png',
          css:'class1p col-11 relativep',
          bgimg:[{
            bgimg:1,
            class:"col-3 no_pd_both fadeinimgp",
            data:'img3bg_v1.png'
          },
          {
            bgimg:2,
            class:"col-3 no_pd_both fadeinimgp",
            data:'img3bg_v2.png'
          },
          {
            bgimg:3,
            class:"col-3 no_pd_both fadeinimgp",
            data:'img3bg_v2.png'
          },
          {
            bgimg:4,
            class:"col-3 no_pd_both fadeinimgp",
            data:'img3bg_v3.png'
          }
        ],
          moreimg:[
            {
              imgno:1,
              class:"first img100 imgboxp animatef",
              data:'img3bg.png'
            },
            {
              imgno:2,
              class:'second img100 imgboxp animates',
              data:'img3item.png'
            },
            {
              imgno:3,
              class:'third img100 imgboxp animatel',
              data:'img3text.png'
            }
          ],
          description1:'productdes3',
          paragraph:[
            {
              index1:1,
              class:'normal',
              data:'productparag3v1'
            },
            {
              index1:2,
              class:'normal',
              data:'productparag3v2'
            },
            {
              index1:3,
              class:'normal',
              data:'productparag3v3'
            },
            {
              index1:4,
              class:'normal',
              data:'productparag3v4'
            }
          ],
          paragraphmore:[
            {
                index1:1,
                class:'boldp',
                data:'productparag3v5'
              },
              {
                index1:2,
                class:'normal',
                data:'productparag3v6'
              },
              {
                index1:3,
                class:'boldp',
                data:'productparag3v7'
              },
              {
                index1:4,
                class:'normal',
                data:'productparag3v8'
              }
          ]
        },
        {
          index:4,
          title:'productType4',
          imgcategory:'web_red.png',
          img:'phone4.png',
          css:'class1p col-11 relativep',
          bgimg:[{
            bgimg:1,
            class:"col-3 no_pd_both fadeinimgp",
            data:'img4bg_v1.png'
          },
          {
            bgimg:2,
            class:"col-3 no_pd_both fadeinimgp",
            data:'img4bg_v2.png'
          },
          {
            bgimg:3,
            class:"col-3 no_pd_both fadeinimgp",
            data:'img4bg_v2.png'
          },
          {
            bgimg:4,
            class:"col-3 no_pd_both fadeinimgp",
            data:'img4bg_v3.png'
          }
        ],
          description1:'productdes4',
          moreimg:[
            {
              imgno:1,
              class:"first img100 imgboxp animatef",
              data:'img4bg.png'
            },
            {
              imgno:2,
              class:'second img100 imgboxp animates',
              data:'img4item.png'
            },
            {
              imgno:3,
              class:'third img100 imgboxp animatel',
              data:'img4text.png'
            }
          ],
          paragraph:[
            {
              index1:1,
              class:'normal',
              data:'productparag4v1'
            },
            {
              index1:2,
              class:'normal',
              data:'productparag4v2'
            },
            {
              index1:3,
              class:'normal',
              data:'productparag4v3'
            },
            {
              index1:4,
              class:'boldp',
              data:'productparag4v4'
            },
            {
              index1:5,
              class:'normal',
              data:'productparag4v5'
            },
            {
              index1:6,
              class:'boldp',
              data:'productparag4v6'
            },
            {
              index1:7,
              class:'normal',
              data:'productparag4v7'
            },
          ],
          paragraphmore:[
            {
              index1:1,
              class:'boldp',
              data:'productparag4v8'
            },
            {
              index1:2,
              class:'normal',
              data:'productparag4v9'
            },
            {
              index1:3,
              class:'normal',
              data:'productparag4v10'
            },
            {
              index1:4,
              class:'normal',
              data:'productparag4v11'
            },
            {
              index1:5,
              class:'normal',
              data:'productparag4v12'
            },
            {
              index1:6,
              class:'normal',
              data:'productparag4v13'
            }
          ]
        },
        {
          index:5,
          title:'productType5',
          imgcategory:'mobile_red.png',
          img:'phone5.png',
          css:'class1p col-11 relativep',
          description1:'productdes5',
          bgimg:[{
            bgimg:1,
            class:"col-3 no_pd_both fadeinimgp",
            data:'img5bg_v1.png'
          },
          {
            bgimg:2,
            class:"col-3 no_pd_both fadeinimgp",
            data:'img5bg_v2.png'
          },
          {
            bgimg:3,
            class:"col-3 no_pd_both fadeinimgp",
            data:'img5bg_v2.png'
          },
          {
            bgimg:4,
            class:"col-3 no_pd_both fadeinimgp",
            data:'img5bg_v3.png'
          }
        ],
          moreimg:[
            {
              imgno:1,
              class:"first img100 imgboxp animatef",
              data:'img5bg.png'
            },
            {
              imgno:2,
              class:'second img100 imgboxp animates',
              data:'img5item.png'
            },
            {
              imgno:3,
              class:'third img100 imgboxp animatel',
              data:'img5text.png'
            }
          ],
          paragraph:[
            {
              index1:1,
              class:'normal',
              data:'productparag5v1'
            },
            {
              index1:2,
              class:'normal',
              data:'productparag5v2'
            }
          ],
          paragraphmore:[
          ]
        },
        {
          index:6,
          title:'productType6',
          imgcategory:'ecome_red.png',
          img:'phone6.png',
          css:'class1p col-11 relativep',
          bgimg:[{
            bgimg:1,
            class:"col-3 no_pd_both fadeinimgp",
            data:'img6bg_v1.png'
          },
          {
            bgimg:2,
            class:"col-3 no_pd_both fadeinimgp",
            data:'img6bg_v2.png'
          },
          {
            bgimg:3,
            class:"col-3 no_pd_both fadeinimgp",
            data:'img6bg_v2.png'
          },
          {
            bgimg:4,
            class:"col-3 no_pd_both fadeinimgp",
            data:'img6bg_v3.png'
          }
        ],
          description1:'productdes6',
          moreimg:[
            {
              imgno:1,
              class:"first img100 imgboxp animatef",
              data:'img6bg.png'
            },
            {
              imgno:2,
              class:'second img100 imgboxp animates',
              data:'img6item.png'
            },
            {
              imgno:3,
              class:'third img100 imgboxp animatel',
              data:'img6text.png'
            }
          ],
          paragraph:[
            {
              index1:1,
              class:'normal',
              data:'productparag6v1'
            },
            {
              index1:2,
              class:'normal',
              data:'productparag6v2'
            },
            {
              index1:3,
              class:'normal',
              data:'productparag6v3'
            },
            {
              index1:4,
              class:'normal',
              data:'productparag6v4'
            }
          ],
          paragraphmore:[
            {
              index1:1,
              class:'boldp',
              data:'productparag6v5'
            },
            {
              index1:2,
              class:'normal',
              data:'productparag6v6'
            },
            {
              index1:3,
              class:'boldp',
              data:'productparag6v7'
            },
            {
              index1:4,
              class:'normal',
              data:'productparag6v8'
            },
            {
              index1:5,
              class:'normal',
              data:'productparag6v9'
            },
            {
              index1:6,
              class:'boldp',
              data:'productparag6v10'
            },
            {
              index1:7,
              class:'normal',
              data:'productparag6V11'
            },
            {
              index1:8,
              class:'listfontp',
              data:'productparag6v12'
            },
            {
              index1:9,
              class:'listfontp',
              data:'productparag6v13'
            },
            {
              index1:10,
              class:'listfontp',
              data:'productparag6v14'
            },
            {
              index1:11,
              class:'listfontp',
              data:'productparag6v15'
            },
            {
              index1:12,
              class:'listfontp',
              data:'productparag6v16'
            },
            {
              index1:13,
              class:'listfontp',
              data:'productparag6v17'
            },
            {
              index1:14,
              class:'listfontp',
              data:'productparag6v18'
            },
            {
              index1:15,
              class:'boldp',
              data:'productparag6v19'
            },
            {
              index1:16,
              class:'normal',
              data:'productparag6v20'
            },
            {
              index1:17,
              class:'normal',
              data:'productparag6v21'
            }
          ]
        },
        {
          index:7,
          title:'productType7',
          imgcategory:'content_red.png',
          img:'phone7.png',
          css:'class1p col-11 relativep',
          description1:'productdes7',
          bgimg:[{
            bgimg:1,
            class:"col-3 no_pd_both fadeinimgp",
            data:'img7bg_v1.png'
          },
          {
            bgimg:2,
            class:"col-3 no_pd_both fadeinimgp",
            data:'img7bg_v2.png'
          },
          {
            bgimg:3,
            class:"col-3 no_pd_both fadeinimgp",
            data:'img7bg_v2.png'
          },
          {
            bgimg:4,
            class:"col-3 no_pd_both fadeinimgp",
            data:'img7bg_v3.png'
          }
        ],
          moreimg:[
            {
              imgno:1,
              class:"first img100 imgboxp animatef",
              data:'img7bg.png'
            },
            {
              imgno:2,
              class:'second img100 imgboxp animates',
              data:'img7item.png'
            },
            {
              imgno:3,
              class:'third img100 imgboxp animatel',
              data:'img7text.png'
            }
          ],
          paragraph:[
            {
              index1:1,
              class:'normal',
              data:'productparag7v1'
            },
            {
              index1:2,
              class:'normal',
              data:'productparag7v2'
            },
            {
              index1:3,
              class:'normal',
              data:'productparag7v3'
            },
            {
              index1:4,
              class:'normal',
              data:'productparag7v4'
            }
          ],
          paragraphmore:[
            {
            }
          ]
        },
        {
          index:8,
          title:'productType8',
          imgcategory:'anti_red.png',
          img:'phone1.png',
          css:'class1p col-11 relativep',
          description1:'productdes8',
          bgimg:[{
            bgimg:1,
            class:"col-3 no_pd_both fadeinimgp",
            data:'img8bg_v1.png'
          },
          {
            bgimg:2,
            class:"col-3 no_pd_both fadeinimgp",
            data:'img8bg_v2.png'
          },
          {
            bgimg:3,
            class:"col-3 no_pd_both fadeinimgp",
            data:'img8bg_v2.png'
          },
          {
            bgimg:4,
            class:"col-3 no_pd_both fadeinimgp",
            data:'img8bg_v3.png'
          }
        ],
        moreimg:[
          {
            imgno:1,
            class:"first img100 imgboxp animatef",
            data:'img8bg.png'
          },
          {
            imgno:2,
            class:'second img100 imgboxp animates',
            data:'img8item.png'
          },
          {
            imgno:3,
            class:'third img100 imgboxp animatel',
            data:'img8text.png'
          }
        ],
          paragraph:[
            {
              index1:1,
              class:'normal',
              data:'productparag8v1'
            },
            {
              index1:2,
              class:'normal',
              data:'productparag8v2'
            },
            {
              index1:3,
              class:'normal',
              data:'productparag8v3'
            },
            {
              index1:4,
              class:'normal',
              data:'productparag8v4'
            }
          ],
          paragraphmore:[
            {
            }
          ]
        },
        {
          index:9,
          title:'productType9',
          imgcategory:'consut_red.png',
          img:'phone1.png',
          css:'class1p col-11 relativep',
          description1:'productdes9',
          bgimg:[{
            bgimg:1,
            class:"col-3 no_pd_both fadeinimgp",
            data:'img9bg_v1.png'
          },
          {
            bgimg:2,
            class:"col-3 no_pd_both fadeinimgp",
            data:'img9bg_v2.png'
          },
          {
            bgimg:3,
            class:"col-3 no_pd_both fadeinimgp",
            data:'img9bg_v2.png'
          },
          {
            bgimg:4,
            class:"col-3 no_pd_both fadeinimgp",
            data:'img9bg_v3.png'
          }
        ],
        moreimg:[
          {
            imgno:1,
            class:"first img100 imgboxp animatef",
            data:'img9bg.png'
          },
          {
            imgno:2,
            class:'second img100 imgboxp animates',
            data:'img9item.png'
          },
          {
            imgno:3,
            class:'third img100 imgboxp animatel',
            data:'img9text.png'
          }
        ],
          paragraph:[
            {
              index1:1,
              class:'normal',
              data:'productparag2v1'
            },
            {
              index1:2,
              class:'normal',
              data:'productparag2v2'
            },
            {
              index1:3,
              class:'normal',
              data:'productparag2v3'
            },
            {
              index1:4,
              class:'normal',
              data:'productparag2v4'
            }
          ],
          paragraphmore:[
            {

            }
          ]
        }

      ]
    }
  },
created() {
    window.addEventListener('resize', this.handleResize)
    this.handleResize();
    this.type = this.$route.params.type;
    this.selectitem=this.categorywork.find(x => x.index == this.type);
},
destroy() {
  window.removeEventListener('resize', this.handleResize)
},
watch: {
  '$route.params.type': function (type) {
    this.type = this.$route.params.type;
    this.selectitem=this.categorywork.find(x => x.index == this.type);
    window.scrollTo(0,0);
  }
},
 methods: {
  handleResize() {
    this.window.width = window.innerWidth;
    this.window.height = window.innerHeight;
    if(this.window.width < 376)
    {
      this.cssWork.part1height=this.window.width
      this.cssWork.part1height1=this.window.width
      this.cssWork.extrapadding=5
      this.cssWork.extrapadding1=30
    }
   else if(this.window.width < 415)
    {
      this.cssWork.part1height=this.window.width
      this.cssWork.part1height1=this.window.width
      this.cssWork.extrapadding=5
      this.cssWork.extrapadding1=30
    }
    else  if(this.window.width < 769)
    {
      this.cssWork.part1height=this.window.width/1.8
      this.cssWork.part1height1=this.window.width
      this.cssWork.extrapadding=5
      this.cssWork.extrapadding1=20
    }
   else
    {
      this.cssWork.part1height=this.window.width
      this.cssWork.part1height1=this.window.width
      this.cssWork.extrapadding=5
      this.cssWork.col='col-6'
      this.cssWork.extrapadding1=25
    }
  },
  next(item){
    if(item==9)
    {
      item=1
      this.selectitem=this.categorywork.find(x => x.index == item);
    }
    else
    {
      item+=1
      this.selectitem=this.categorywork.find(x => x.index == item);
    }
  },
  prev(item){
    if(item==1)
    {
      item=9
      this.selectitem=this.categorywork.find(x => x.index == item);
    }
    else
    {
      item-=1
    this.selectitem=this.categorywork.find(x => x.index == item);
    }
  }
},
}

