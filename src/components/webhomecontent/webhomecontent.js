const now = new Date();
const year = now.getFullYear();
export default {
    data () {
      return {
        year,
        slide: 0,
        onHover:false,
        sliding: null,
        img1:'product1.png',
        img2:'product1.png',
        img3:'product1.png',
        img4:'product1.png',
        img5:'product1.png',
        img6:'product1.png',
        img7:'product1.png',
        img8:'product1.png',
        img9:'product1.png',
        scrollPosition: null,
        hover:0,
        file2:'',
        cssHome:{
          part1height:0,
        },
        selected: [], // Must be an array reference!
        options: [
        {
          text: 'Mr',
          value: 'M'
        },
        {
          text: 'Ms',
          value: 'F'
        }
        ],
        items: [
        {
          text: 'HOME',
          href: '/home'
        }, {
          text: 'JOB',
          href: ''
        }, {
          text: 'APPLY FOR A JOB',
          active: true
        }
        ],
        form: {
            email: '',
            name: '',
            food: null,
            checked: []
          },
        foods: [
          {
            text: 'Select One',
            value: null
          }, 'Carrots', 'Beans', 'Tomatoes', 'Corn'
        ],
        show: true,
        options_freelance: [
          {
            value: 'Yes',
            text: 'Yes',
            selected: true
          },
          {
            value: 'No',
            text: 'No'
          }
        ],
        options_expertise: [{
            value: 'SG',
            text: 'Sinpapore',
            selected: true
          },
          {
            value: 'No',
            text: 'No'
          }
        ],
        options_country: [{
            value: 'MY',
            text: 'Malaysia',
            selected: true
          },
          {
            value: 'SG',
            text: 'Singapore'
          }
        ],
        expectwork:[
          {
            csslevel1:'product11 productboxitem',
            name:'productType1',
            index:1,
          animationdelaytime:300,
            img:'product1.png',
            nonactive:'product1.png',
            active:'hoverproduct1.png'
          },
          {
            csslevel1:'product22 productboxitem',
            name:'productType2',
            index:2,
            animationdelaytime:600,
            img:'product2.png',
            nonactive:'product2.png',
            active:'hoverproduct2.png'
          },
          {
            csslevel1:'product33 productboxitem',
            name:'productType3',
            index:3,
            animationdelaytime:900,
            img:'product3.png',
            nonactive:'product3.png',
            active:'hoverproduct3.png'
          },
          {
            csslevel1:'product44 productboxitem',
            name:'productType4',
            index:4,
            animationdelaytime:300,
            img:'product4.png',
            nonactive:'product4.png',
            active:'hoverproduct4.png'
      
          },
          {
            csslevel1:'product55 productboxitem',
            name:'productType5',
            index:5,
            animationdelaytime:300,
            img:'product5.png',
            nonactive:'product5.png',
            active:'hoverproduct5.png'
      
          },
          {
            csslevel1:'product66 productboxitem',
            name:'productType6',
            index:6,
            animationdelaytime:300,    
            img:'product6.png',
            nonactive:'product6.png',
            active:'hoverproduct6.png'
      
          },
          {
            csslevel1:'product77 productboxitem',
            name:'productType7',
            index:7,
            animationdelaytime:300,
            img:'product7.png',
            nonactive:'product7.png',
            active:'hoverproduct7.png'
      
          },
          {
            csslevel1:'product88 productboxitem',
            name:'productType8',
            index:8,
            animationdelaytime:300,
            img:'product8.png',
            nonactive:'product8.png',
            active:'hoverproduct8.png'
      
          },
          {
            csslevel1:'product99 productboxitem',
            name:'productType9',
            index:9,
            animationdelaytime:300,
            img:'product9.png',
            nonactive:'product9.png',
            active:'hoverproduct9.png'
      
          }
        ],
        window: {
          width: 0,
          height: 0
        },
        selected: [], // Must be an array reference!
        options: [
          { text: 'Subscribe to newsletter', value: 'yes' },
        ]
      }
    },
    methods: {
      updateScroll() {
        this.scrollPosition = window.scrollY
      },
      handleScroll (event) {

        // Any code to be executed when the window is scrolled
        this.scrollPosition = window.scrollY
        if(this.scrollPosition < 400 )
        {
          this.expectwork=[
            {
              csslevel1:'product11 productboxitem',
              name:'productType1',
              index:1,
            animationdelaytime:300,
              img:'product1.png',
              nonactive:'product1.png',
              active:'hoverproduct1.png'
            },
            {
              csslevel1:'product22 productboxitem',
              name:'productType2',
              index:2,
              animationdelaytime:600,
              img:'product2.png',
              nonactive:'product2.png',
              active:'hoverproduct2.png'
            },
            {
              csslevel1:'product33 productboxitem',
              name:'productType3',
              index:3,
              animationdelaytime:900,
              img:'product3.png',
              nonactive:'product3.png',
              active:'hoverproduct3.png'
            },
            {
              csslevel1:'product44 productboxitem',
              name:'productType4',
              index:4,
              animationdelaytime:300,
              img:'product4.png',
              nonactive:'product4.png',
              active:'hoverproduct4.png'
        
            },
            {
              csslevel1:'product55 productboxitem',
              name:'productType5',
              index:5,
              animationdelaytime:300,
              img:'product5.png',
              nonactive:'product5.png',
              active:'hoverproduct5.png'
        
            },
            {
              csslevel1:'product66 productboxitem',
              name:'productType6',
              index:6,
              animationdelaytime:300,    
              img:'product6.png',
              nonactive:'product6.png',
              active:'hoverproduct6.png'
        
            },
            {
              csslevel1:'product77 productboxitem',
              name:'productType7',
              index:7,
              animationdelaytime:300,
              img:'product7.png',
              nonactive:'product7.png',
              active:'hoverproduct7.png'
        
            },
            {
              csslevel1:'product88 productboxitem',
              name:'productType8',
              index:8,
              animationdelaytime:300,
              img:'product8.png',
              nonactive:'product8.png',
              active:'hoverproduct8.png'
        
            },
            {
              csslevel1:'product99 productboxitem',
              name:'productType9',
              index:9,
              animationdelaytime:300,
              img:'product9.png',
              nonactive:'product9.png',
              active:'hoverproduct9.png'
        
            }
          ]
       }
        else if (this.scrollPosition >700)
        {
          this.expectwork=[
            {
              csslevel1:'product11 productboxitem',
              name:'productType1',
              index:1,
            animationdelaytime:300,
              img:'product1.png',
              nonactive:'product1.png',
              active:'hoverproduct1.png'
            },
            {
              csslevel1:'product22 productboxitem',
              name:'productType2',
              index:2,
              animationdelaytime:600,
              img:'product2.png',
              nonactive:'product2.png',
              active:'hoverproduct2.png'
            },
            {
              csslevel1:'product33 productboxitem',
              name:'productType3',
              index:3,
              animationdelaytime:900,
              img:'product3.png',
              nonactive:'product3.png',
              active:'hoverproduct3.png'
            },
            {
              csslevel1:'product44 productboxitem',
              name:'productType4',
              index:4,
              animationdelaytime:300,
              img:'product4.png',
              nonactive:'product4.png',
              active:'hoverproduct4.png'
        
            },
            {
              csslevel1:'product55 productboxitem',
              name:'productType5',
              index:5,
              animationdelaytime:300,
              img:'product5.png',
              nonactive:'product5.png',
              active:'hoverproduct5.png'
        
            },
            {
              csslevel1:'product66 productboxitem',
              name:'productType6',
              index:6,
              animationdelaytime:300,    
              img:'product6.png',
              nonactive:'product6.png',
              active:'hoverproduct6.png'
        
            },
            {
              csslevel1:'product77 productboxitem',
              name:'productType7',
              index:7,
              animationdelaytime:300,
              img:'product7.png',
              nonactive:'product7.png',
              active:'hoverproduct7.png'
        
            },
            {
              csslevel1:'product88 productboxitem',
              name:'productType8',
              index:8,
              animationdelaytime:300,
              img:'product8.png',
              nonactive:'product8.png',
              active:'hoverproduct8.png'
        
            },
            {
              csslevel1:'product99 productboxitem',
              name:'productType9',
              index:9,
              animationdelaytime:300,
              img:'product9.png',
              nonactive:'product9.png',
              active:'hoverproduct9.png'
        
            }
          ]

        }
      },
      onSlideStart(slide) {
        this.sliding = true
      },
      scrollToTop() {
        window.scrollTo(0,0);
      },
      onSlideEnd(slide) {
        this.sliding = false
      },
      mouseover(item){
          item.img = item.active
          this.hover=item.index
          return item
      },
      handleResize() {
        this.window.width = window.innerWidth;
        this.window.height = window.innerHeight;
        if(this.window.height)
        {
          this.cssHome.part1height=this.window.height*(9.5/10)
        }
      },
      mouseleave(item){
        item.img = item.nonactive
        this.hover=0

        return item
      },
      imageRotate(arr, reverse) {
        if (reverse) arr.unshift(arr.pop());
        else arr.push(arr.shift());
        return arr;
      },
      moveLeft() {
        if (this.onHover) {
          return;
        }
        this.state = 'right';
        this.imageRotate(this.expectwork, true);
      },
      moveRight() {
        if (this.onHover) {
          return;
        }
        this.state = 'left';
        this.imageRotate(this.expectwork, false);
      },
      stop(){
        clearInterval(this.timer);
      },
      onFinish($event) {
        this.state = 'void';
        this.onHover = false;
      },
      onStart($event) {
        this.onHover = true;
      },
      start() {
        this.timer=setInterval(() => {
          if (this.expectwork.length > 4) {
            this.moveRight();
          }
        }, 5000);
      },
      openlink(url) {
        this.$router.push(url)
      }
    },
    mounted() {
      window.addEventListener('scroll', this.handleScroll);
      this.start()
    },
    destroy() {
      window.removeEventListener('scroll', this.handleScroll)
      window.removeEventListener('resize', this.handleResize)
      this.stop()
    },
    created() {
      window.addEventListener('resize', this.handleResize)
      window.addEventListener('scroll', this.handleScroll);
      this.handleResize();
    }
  }
