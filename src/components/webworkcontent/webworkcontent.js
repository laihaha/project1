const now = new Date();
const year = now.getFullYear();
export default {
  data() {
    return {
      msg: 'Welcome to Your Vue.js App',
      year,
      type:null,
      selectitem:[],    
      window: {
        width: 0,
        height: 0
      },
      cssWork:{
        part1height:0,
        part1height1:0,
        extrapadding:0,
        extrapadding1:0,
        col:'col-6',
      },
      categorywork:[
        {
          index:1,
          title:'productType1',
          imgcategory:'strategi_red.png',
          img:'bulb.png',
          css:'class1 col-9 relative',
          description1:'productdes1',
          bgimg:[{
            bgimg:1,
            class:"col-3 no_pd_both fadeinimg",
            data:'img1bg_v1.png'
          },
          {
            bgimg:2,
            class:"col-3 no_pd_both fadeinimg",
            data:'img1bg_v2.png'
          },
          {
            bgimg:3,
            class:"col-3 no_pd_both fadeinimg",
            data:'img1bg_v3.png'
          },
          {
            bgimg:4,
            class:"col-3 no_pd_both fadeinimg",
            data:'img1bg_v4.png'
          }
        ],
        moreimg:[
          {
            imgno:1,
            class:"firstw img100 imgboxp animatefw",
            data:'img1bg.png'
          },
          {
            imgno:2,
            class:'secondw img100 imgboxp animatesw',
            data:'img1item.png'
          },
          {
            imgno:3,
            class:'thirdw img100 imgboxp animatelw',
            data:'img1text.png'
          }
        ],
          paragraph:[
            {
              index1:1,
              data:'productparag1v1'
            },
            {
              index1:2,
              data:'productparag1v2'
            },
            {
              index1:3,
              data:'productparag1v3'
            },
            {
              index1:4,
              data:'productparag1v4'
            }
          ],
          paragraphmore:[
          ]
        },
        {
          index:2,
          title:'productType2',
          imgcategory:'branding_red.png',
          img:'web1.png',
          css:'class1 col-9 relative',
          description1:'productdes2',
          bgimg:[{
            bgimg:1,
            class:"col-3 no_pd_both fadeinimg",
            data:'img2bg_v1.png'
          },
          {
            bgimg:2,
            class:"col-3 no_pd_both fadeinimg",
            data:'img2bg_v2.png'
          },
          {
            bgimg:3,
            class:"col-3 no_pd_both fadeinimg",
            data:'img2bg_v3.png'
          },
          {
            bgimg:4,
            class:"col-3 no_pd_both fadeinimg",
            data:'img2bg_v4.png'
          }
        ],
        moreimg:[
          {
            imgno:1,
            class:"firstw img100 imgboxp animatefw",
            data:'img2bg.png'
          },
          {
            imgno:2,
            class:'secondw img100 imgboxp animatesw',
            data:'img2item.png'
          },
          {
            imgno:3,
            class:'thirdw img100 imgboxp animatelw',
            data:'img2text.png'
          }
        ],
          paragraph:[
            {
              index1:1,
              class:'normal',
              data:'productparag2v1'
            },
            {
              index1:2,
              class:'normal',
              data:'productparag2v2'
            },
            {
              index1:3,
              class:'normal',
              data:'productparag2v3'
            },
            {
              index1:4,
              class:'normal',
              data:'productparag2v4'
            }
          ],
          paragraphmore:[
          ]
        },
        {
          index:3,
          title:'productType3',
          imgcategory:'digitak_red.png',
          img:'web2.png',
          css:'class1 col-9 relative',
          description1:'productdes3',
          bgimg:[{
            bgimg:1,
            class:"col-3 no_pd_both fadeinimg",
            data:'img3bg_v1.png'
          },
          {
            bgimg:2,
            class:"col-3 no_pd_both fadeinimg",
            data:'img3bg_v2.png'
          },
          {
            bgimg:3,
            class:"col-3 no_pd_both fadeinimg",
            data:'img3bg_v2.png'
          },
          {
            bgimg:4,
            class:"col-3 no_pd_both fadeinimg",
            data:'img3bg_v3.png'
          }
        ],
        moreimg:[
          {
            imgno:1,
            class:"firstw img100 imgboxp animatefw",
            data:'img3bg.png'
          },
          {
            imgno:2,
            class:'secondw img100 imgboxp animatesw',
            data:'img3item.png'
          },
          {
            imgno:3,
            class:'thirdw img100 imgboxp animatelw',
            data:'img3text.png'
          }
        ],
          paragraph:[
            {
              index1:1,
              class:'normal',
              data:'productparag3v1'
            },
            {
              index1:2,
              class:'normal',
              data:'productparag3v2'
            },
            {
              index1:3,
              class:'normal',
              data:'productparag3v3'
            },
            {
              index1:4,
              class:'normal',
              data:'productparag3v4'
            }
          ],
          paragraphmore:[
            {
                index1:1,
                class:'bold',
                data:'productparag3v5'
              },
              {
                index1:2,
                class:'normal',
                data:'productparag3v6'
              },
              {
                index1:3,
                class:'bold',
                data:'productparag3v7'
              },
              {
                index1:4,
                class:'normal',
                data:'productparag3v8'
              }
          ]
        },
        {
          index:4,
          title:'productType4',
          imgcategory:'web_red.png',
          img:'web4.png',
          css:'class1 col-9 relative',
          description1:'productdes4',
          bgimg:[{
            bgimg:1,
            class:"col-3 no_pd_both fadeinimg",
            data:'img4bg_v1.png'
          },
          {
            bgimg:2,
            class:"col-3 no_pd_both fadeinimg",
            data:'img4bg_v2.png'
          },
          {
            bgimg:3,
            class:"col-3 no_pd_both fadeinimg",
            data:'img4bg_v2.png'
          },
          {
            bgimg:4,
            class:"col-3 no_pd_both fadeinimg",
            data:'img4bg_v3.png'
          }
        ],
        moreimg:[
          {
            imgno:1,
            class:"firstw img100 imgboxp animatefw",
            data:'img3bg.png'
          },
          {
            imgno:2,
            class:'secondw img100 imgboxp animatesw',
            data:'img3item.png'
          },
          {
            imgno:3,
            class:'thirdw img100 imgboxp animatelw',
            data:'img3text.png'
          }
        ],
          paragraph:[
            {
              index1:1,
              class:'normal',
              data:'productparag4v1'
            },
            {
              index1:2,
              class:'normal',
              data:'productparag4v2'
            },
            {
              index1:3,
              class:'normal',
              data:'productparag4v3'
            },
            {
              index1:4,
              class:'bold',
              data:'productparag4v4'
            },
            {
              index1:5,
              class:'normal',
              data:'productparag4v5'
            },
            {
              index1:6,
              class:'bold',
              data:'productparag4v6'
            },  
            {
              index1:7,
              class:'normal',
              data:'productparag4v7'
            },
          ],
          paragraphmore:[
            {
              index1:1,
              class:'bold',
              data:'productparag4v8'
            },
            {
              index1:2,
              class:'normal',
              data:'productparag4v9'
            },
            {
              index1:3,
              class:'normal',
              data:'productparag4v10'
            },
            {
              index1:4,
              class:'normal',
              data:'productparag4v11'
            },
            {
              index1:5,
              class:'normal',
              data:'productparag4v12'
            },
            {
              index1:6,
              class:'normal',
              data:'productparag4v13'
            }
          ]
        },
        {
          index:5,
          title:'productType5',
          imgcategory:'mobile_red.png',
          img:'web5.png',
          css:'class1 col-9 relative',
          bgimg:[{
            bgimg:1,
            class:"col-3 no_pd_both fadeinimg",
            data:'img5bg_v1.png'
          },
          {
            bgimg:2,
            class:"col-3 no_pd_both fadeinimg",
            data:'img5bg_v2.png'
          },
          {
            bgimg:3,
            class:"col-3 no_pd_both fadeinimg",
            data:'img5bg_v2.png'
          },
          {
            bgimg:4,
            class:"col-3 no_pd_both fadeinimg",
            data:'img5bg_v3.png'
          }
        ],
        moreimg:[
          {
            imgno:1,
            class:"firstw img100 imgboxp animatefw",
            data:'img5bg.png'
          },
          {
            imgno:2,
            class:'secondw img100 imgboxp animatesw',
            data:'img5item.png'
          },
          {
            imgno:3,
            class:'thirdw img100 imgboxp animatelw',
            data:'img5text.png'
          }
        ],
          description1:'productdes5',
          paragraph:[
            {
              index1:1,
              class:'normal',
              data:'productparag5v1'
            },
            {
              index1:2,
              class:'normal',
              data:'productparag5v2'
            }
          ],
          paragraphmore:[
          ]
        },
        {
          index:6,
          title:'productType6',
          imgcategory:'ecome_red.png',
          img:'web6.png',
          css:'class1 col-9 relative',
          description1:'productdes6',
          bgimg:[{
            bgimg:1,
            class:"col-3 no_pd_both fadeinimg",
            data:'img6bg_v1.png'
          },
          {
            bgimg:2,
            class:"col-3 no_pd_both fadeinimg",
            data:'img6bg_v2.png'
          },
          {
            bgimg:3,
            class:"col-3 no_pd_both fadeinimg",
            data:'img6bg_v2.png'
          },
          {
            bgimg:4,
            class:"col-3 no_pd_both fadeinimg",
            data:'img6bg_v3.png'
          }
        ],
        moreimg:[
          {
            imgno:1,
            class:"firstw img100 imgboxp animatefw",
            data:'img6bg.png'
          },
          {
            imgno:2,
            class:'secondw img100 imgboxp animatesw',
            data:'img6item.png'
          },
          {
            imgno:3,
            class:'thirdw img100 imgboxp animatelw',
            data:'img6text.png'
          }
        ],
          paragraph:[
            {
              index1:1,
              class:'normal',
              data:'productparag6v1'
            },
            {
              index1:2,
              class:'normal',
              data:'productparag6v2'
            },
            {
              index1:3,
              class:'normal',
              data:'productparag6v3'
            },
            {
              index1:4,
              class:'normal',
              data:'productparag6v4'
            }
          ],
          paragraphmore:[
            {
              index1:1,
              class:'bold',
              data:'productparag6v5'
            },
            {
              index1:2,
              class:'normal',
              data:'productparag6v6'
            },
            {
              index1:3,
              class:'bold',
              data:'productparag6v7'
            },
            {
              index1:4,
              class:'normal',
              data:'productparag6v8'
            },
            {
              index1:5,
              class:'normal',
              data:'productparag6v9'
            },
            {
              index1:6,
              class:'bold',
              data:'productparag6v10'
            },
            {
              index1:7,
              class:'normal',
              data:'productparag6V11'
            },
            {
              index1:8,
              class:'listfont',
              data:'productparag6v12'
            },    
            {
              index1:9,
              class:'listfont',
              data:'productparag6v13'
            },
            {
              index1:10,
              class:'listfont',
              data:'productparag6v14'
            },
            {
              index1:11,
              class:'listfont',
              data:'productparag6v15'
            },
            {
              index1:12,
              class:'listfont',
              data:'productparag6v16'
            },
            {
              index1:13,
              class:'listfont',
              data:'productparag6v17'
            },
            {
              index1:14,
              class:'listfont',
              data:'productparag6v18'
            },
            {
              index1:15,
              class:'bold1',
              data:'productparag6v19'
            },
            {
              index1:16,
              class:'normal',
              data:'productparag6v20'
            },
            {
              index1:17,
              class:'normal',
              data:'productparag6v21'
            }
          ]
        },
        {
          index:7,
          title:'productType7',
          imgcategory:'content_red.png',
          img:'web7.png',
          css:'class1 col-9 relative',
          description1:'productdes7',
          bgimg:[{
            bgimg:1,
            class:"col-3 no_pd_both fadeinimg",
            data:'img7bg_v1.png'
          },
          {
            bgimg:2,
            class:"col-3 no_pd_both fadeinimg",
            data:'img7bg_v2.png'
          },
          {
            bgimg:3,
            class:"col-3 no_pd_both fadeinimg",
            data:'img7bg_v2.png'
          },
          {
            bgimg:4,
            class:"col-3 no_pd_both fadeinimg",
            data:'img7bg_v3.png'
          }
        ],
        moreimg:[
          {
            imgno:1,
            class:"firstw img100 imgboxp animatefw",
            data:'img7bg.png'
          },
          {
            imgno:2,
            class:'secondw img100 imgboxp animatesw',
            data:'img7item.png'
          },
          {
            imgno:3,
            class:'thirdw img100 imgboxp animatelw',
            data:'img7text.png'
          }
        ],
          paragraph:[
            {
              index1:1,
              class:'normal',
              data:'productparag7v1'
            },
            {
              index1:2,
              class:'normal',
              data:'productparag7v2'
            },
            {
              index1:3,
              class:'normal',
              data:'productparag7v3'
            },
            {
              index1:4,
              class:'normal',
              data:'productparag7v4'
            }
          ],
          paragraphmore:[
            {
            }
          ]
        },
        {
          index:8,
          title:'productType8',
          imgcategory:'anti_red.png',
          img:'web1.png',
          css:'class1 col-9 relative',
          bgimg:[{
            bgimg:1,
            class:"col-3 no_pd_both fadeinimg",
            data:'img8bg_v1.png'
          },
          {
            bgimg:2,
            class:"col-3 no_pd_both fadeinimg",
            data:'img8bg_v2.png'
          },
          {
            bgimg:3,
            class:"col-3 no_pd_both fadeinimg",
            data:'img8bg_v2.png'
          },
          {
            bgimg:4,
            class:"col-3 no_pd_both fadeinimg",
            data:'img8bg_v3.png'
          }
        ],
        moreimg:[
          {
            imgno:1,
            class:"firstw img100 imgboxp animatefw",
            data:'img8bg.png'
          },
          {
            imgno:2,
            class:'secondw img100 imgboxp animatesw',
            data:'img8item.png'
          },
          {
            imgno:3,
            class:'thirdw img100 imgboxp animatelw',
            data:'img8text.png'
          }
        ],
          description1:'productdes8',
          paragraph:[
            {
              index1:1,
              class:'normal',
              data:'productparag8v1'
            },
            {
              index1:2,
              class:'normal',
              data:'productparag8v2'
            },
            {
              index1:3,
              class:'normal',
              data:'productparag8v3'
            },
            {
              index1:4,
              class:'normal',
              data:'productparag8v4'
            }
          ],
          paragraphmore:[
            {
            }
          ]
        },
        {
          index:9,
          title:'productType9',
          imgcategory:'consut_red.png',
          img:'web1.png',
          css:'class1 col-9 relative',
          description1:'productdes9',
          bgimg:[{
            bgimg:1,
            class:"col-3 no_pd_both fadeinimg",
            data:'img9bg_v1.png'
          },
          {
            bgimg:2,
            class:"col-3 no_pd_both fadeinimg",
            data:'img9bg_v2.png'
          },
          {
            bgimg:3,
            class:"col-3 no_pd_both fadeinimg",
            data:'img9bg_v2.png'
          },
          {
            bgimg:4,
            class:"col-3 no_pd_both fadeinimg",
            data:'img9bg_v3.png'
          }
        ],
        moreimg:[
          {
            imgno:1,
            class:"firstw img100 imgboxp animatefw",
            data:'img9bg.png'
          },
          {
            imgno:2,
            class:'secondw img100 imgboxp animatesw',
            data:'img9item.png'
          },
          {
            imgno:3,
            class:'thirdw img100 imgboxp animatelw',
            data:'img9text.png'
          }
        ],
          paragraph:[
            {
              index1:1,
              class:'normal',
              data:'productparag2v1'
            },
            {
              index1:2,
              class:'normal',
              data:'productparag2v2'
            },
            {
              index1:3,
              class:'normal',
              data:'productparag2v3'
            },
            {
              index1:4,
              class:'normal',
              data:'productparag2v4'
            }
          ],
          paragraphmore:[
            {

            }
          ]
        }

      ]
    }
  },
created() {
    window.addEventListener('resize', this.handleResize)
    this.handleResize();
    this.type = this.$route.params.type;   
    this.selectitem=this.categorywork.find(x => x.index == this.type);
},    
destroy() {
  window.removeEventListener('resize', this.handleResize)
},  
watch: {
  '$route.params.type': function (type) {
    this.type = this.$route.params.type;   
    this.selectitem=this.categorywork.find(x => x.index == this.type);
    window.scrollTo(0,0);
  }
},
 methods: { 
  handleResize() {
    this.window.width = window.innerWidth;
    this.window.height = window.innerHeight;
    if(this.window.width < 1367)
    {
      this.cssWork.part1height=this.window.height*(9/10)
      this.cssWork.part1height1=this.window.height*1.25
      this.cssWork.extrapadding=25
      this.cssWork.extrapadding1=5
    }
   else if(this.window.width < 1441)
    {
      this.cssWork.part1height=this.window.height*(8.5/10)
      this.cssWork.part1height1=this.window.height*1.05
      this.cssWork.extrapadding=25
      this.cssWork.extrapadding1=5
    }
    else  if(this.window.width < 1981)
    {
      this.cssWork.part1height=this.window.height*(9/10)
      this.cssWork.part1height1=this.window.height
      this.cssWork.extrapadding=15
      this.cssWork.extrapadding1=7.5
    }
   else 
    {
      this.cssWork.part1height=this.window.height*(7.5/10)
      this.cssWork.part1height1=this.window.height*(7.5/10)
      this.cssWork.col='col-6'
      this.cssWork.extrapadding=10
      this.cssWork.extrapadding1=5
    } 
  },
  next(item){
    if(item==9)
    {
      item=1
      this.selectitem=this.categorywork.find(x => x.index == item);
    }
    else
    {
      item+=1
      this.selectitem=this.categorywork.find(x => x.index == item);
    }
  },
  prev(item){
    if(item==1)
    {
      item=9
      this.selectitem=this.categorywork.find(x => x.index == item);
    }
    else
    {
      item-=1
    this.selectitem=this.categorywork.find(x => x.index == item);
    }
  }
},
}
