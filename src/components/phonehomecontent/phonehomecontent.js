import Csmodal from '../ContactUsModal/ContactUsModal.vue'
import { serverBus } from '../../main.js'
const now = new Date();
const year = now.getFullYear();
export default {
  component:{
    Csmodal,
  },
    data () {
      return {
        msg: 'Welcome to Your Vue.js App',
        year,
        focusedIndex: 0,
        isopen:0,
        modalOpen:false,
        isVisible: false,
        isModalVisible: false,
        slide: 0,
        sliding: null,
        hover:0,
        img1:'product1.png',
        img2:'product1.png',
        img3:'product1.png',
        img4:'product1.png',
        img5:'product1.png',
        img6:'product1.png',
        img7:'product1.png',
        img8:'product1.png',
        img9:'product1.png',
        expectwork:[
          {
            csslevel1:'product11 productboxitem',
            name:'productType1',
            index:1,
          animationdelaytime:300,
            img:'product1.png',
            nonactive:'product1.png',
            active:'hoverproduct1.png'
          },
          {
            csslevel1:'product22 productboxitem',
            name:'productType2',
            index:2,
            animationdelaytime:600,
            img:'product2.png',
            nonactive:'product2.png',
            active:'hoverproduct2.png'
          },
          {
            csslevel1:'product33 productboxitem',
            name:'productType3',
            index:3,
            animationdelaytime:900,
            img:'product3.png',
            nonactive:'product3.png',
            active:'hoverproduct3.png'
          },
          {
            csslevel1:'product44 productboxitem',
            name:'productType4',
            index:4,
            animationdelaytime:300,
            img:'product4.png',
            nonactive:'product4.png',
            active:'hoverproduct4.png'
      
          },
          {
            csslevel1:'product55 productboxitem',
            name:'productType5',
            index:5,
            animationdelaytime:300,
            img:'product5.png',
            nonactive:'product5.png',
            active:'hoverproduct5.png'
      
          },
          {
            csslevel1:'product66 productboxitem',
            name:'productType6',
            index:6,
            animationdelaytime:300,    
            img:'product6.png',
            nonactive:'product6.png',
            active:'hoverproduct6.png'
      
          },
          {
            csslevel1:'product77 productboxitem',
            name:'productType7',
            index:7,
            animationdelaytime:300,
            img:'product7.png',
            nonactive:'product7.png',
            active:'hoverproduct7.png'
      
          },
          {
            csslevel1:'product88 productboxitem',
            name:'productType8',
            index:8,
            animationdelaytime:300,
            img:'product8.png',
            nonactive:'product8.png',
            active:'hoverproduct8.png'
      
          },
          {
            csslevel1:'product99 productboxitem',
            name:'productType9',
            index:9,
            animationdelaytime:300,
            img:'product9.png',
            nonactive:'product9.png',
            active:'hoverproduct9.png'
      
          }
        ],
        window: {
          width: 0,
          height: 0
        },
        selected: [], // Must be an array reference!
        options: [
        {
          text: 'Mr',
          value: 'M'
        },
        {
          text: 'Ms',
          value: 'F'
        }
        ],
        items: [
        {
          text: 'HOME',
          href: '/home'
        }, {
          text: 'JOB',
          href: ''
        }, {
          text: 'APPLY FOR A JOB',
          active: true
        }
        ],
        form: {
            email: '',
            name: '',
            food: null,
            checked: []
          },
        foods: [
          {
            text: 'Select One',
            value: null
          }, 'Carrots', 'Beans', 'Tomatoes', 'Corn'
        ],
        show: true,
        options_freelance: [
          {
            value: 'Yes',
            text: 'Yes',
            selected: true
          },
          {
            value: 'No',
            text: 'No'
          }
        ],
        options_expertise: [{
            value: 'SG',
            text: 'Sinpapore',
            selected: true
          },
          {
            value: 'No',
            text: 'No'
          }
        ],
        options_country: [{
            value: 'MY',
            text: 'Malaysia',
            selected: true
          },
          {
            value: 'SG',
            text: 'Singapore'
          }
        ],
        cssHome:{
          part1height:0,
          part1height1:0,
          extrapadding:0,
        },
        scrollPosition: null,
      }
    },
    methods: {
      onSlideStart(slide) {
        this.sliding = true
      },
      onSlideEnd(slide) {
        this.sliding = false
      },
      showModal() {
        serverBus.$emit('serverSelected2',!this.isModalVisible)
      },
      closeModal() {
        this.isModalVisible = false;
      },
      handleResize() {
        this.window.width = window.innerWidth;
        this.window.height = window.innerHeight;
        if(this.window.width < 370)
        {
          this.cssHome.part1height=this.window.width*3
          this.cssHome.part1height1=this.window.width*1.5
          this.cssHome.extrapadding=0
        }
       else if(this.window.width < 500)
        {
          this.cssHome.part1height=this.window.width*2.5
          this.cssHome.part1height1=this.window.width*1.8
          this.cssHome.extrapadding=0
        }
       else if(this.window.width < 600 )
        {
          this.cssHome.part1height=this.window.width*2
          this.cssHome.part1height1=this.window.width
          this.cssHome.extrapadding=0
        }else if(this.window.width > 1023)
        {
          this.cssHome.part1height=this.window.width
          this.cssHome.part1height1=this.window.width
          this.cssHome.extrapadding=20
        } else {
          {
            this.cssHome.part1height=this.window.width*1.5
            this.cssHome.part1height1=this.window.width*1.25
            this.cssHome.extrapadding=15
          }
        }
      },
    openlink(item) {
        this.$router.push(item)
    }
    },
    mounted() {
    },
    destroy() {
      window.removeEventListener('resize', this.handleResize)
    },
    created() {
      window.addEventListener('resize', this.handleResize)
      this.handleResize();
    }
  }
