import WebMenuBar from '../WebMenuBar/WebMenuBar'
import { serverBus } from '../../main.js'
import i18n from '../../share/i18n'
export default {
  component:{
    WebMenuBar
  },
  mounted() {
    this.menuItems = document.querySelectorAll('.MegaMenu a')
  },
  data() {
    return {
      isVisible: false,
      menuItems: null,
      focusedIndex: 0,
      modalOpen:false,
      isopen:0,
      languages: [
        { flag: 'us', language: 'en', title: 'English', img: '@/assets/img/logo/mainlogo.png' },
        { flag: 'cn', language: 'cn', title: 'Chinese', img: '@/assets/img/bgimg/china_flag.png' }        ]
    }
  },
  methods: {
    openmodal(){
      serverBus.$emit('serverSelected', !this.modalOpen)
      serverBus.$emit('serverSelected1', this.isopen=+1)
    },
    showMenu() {
      this.isVisible = true
    },
    hideMenu() {
      this.isVisible = false
      this.focusedIndex = 0
    },
    startArrowKeys() {
      this.menuItems[0].focus()
    },
    focusPrevious(isArrowKey) {
      this.focusedIndex = this.focusedIndex - 1

      if (isArrowKey) {
        this.focusItem()
      }
    },
    focusNext(isArrowKey) {
      this.focusedIndex = this.focusedIndex + 1

      if (isArrowKey) {
        this.focusItem()
      }
    },
    focusItem() {
      this.menuItems[this.focusedIndex].focus()
    },
    changeLocale(locale) {
        i18n.locale = locale;
    }
  }
}
