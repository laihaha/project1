export default {
  name: 'modal',
  data () {
    return {
      isVisible: false,
      focusedIndex: 0,
      file2:'',
      options: [
      {
        text: 'Mr',
        value: 'M'
      },
      {
        text: 'Ms',
        value: 'F'
      }
      ],
      items: [
      {
        text: 'HOME',
        href: '/home'
      }, {
        text: 'JOB',
        href: ''
      }, {
        text: 'APPLY FOR A JOB',
        active: true
      }
      ],
      form: {
        email: '',
        name: '',
        food: null,
        checked: []
      },
      foods: [
      {
        text: 'Select One',
        value: null
      }, 'Carrots', 'Beans', 'Tomatoes', 'Corn'
      ],
      show: true,
      options_freelance: [
      {
        value: 'Yes',
        text: 'Yes',
        selected: true
      },
      {
        value: 'No',
        text: 'No'
      }
      ],
      options_expertise: [{
        value: 'SG',
        text: 'Sinpapore',
        selected: true
      },
      {
        value: 'No',
        text: 'No'
      }
      ],
      options_country: [{
        value: 'MY',
        text: 'Malaysia',
        selected: true
      },
      {
        value: 'SG',
        text: 'Singapore'
      }
      ],
      options: [
      { text: 'Subscribe to newsletter', value: 'yes' },
      ]
    }
  },
  methods: {
    close() {
      this.$emit('close');
    },
  },
};