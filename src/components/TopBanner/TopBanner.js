export default {
  name: 'TopBanner',
  props: {
    page_title: {
      type: String
    },
    items: {
      type: Array
    }
  }
}
