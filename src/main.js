// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import './plugins/fontawesome'
import App from './App.vue'
import router from './router'
import i18n from './share/i18n'
import baseServ from './share/base.service'
import vueHeadful from 'vue-headful'
import {
  BootstrapVue,
  ModalPlugin
} from 'bootstrap-vue'
import 'aos/dist/aos.css'
import AOS from 'aos'

Vue.config.productionTip = false

Vue.use(BootstrapVue)
Vue.use(ModalPlugin)
Vue.component('vue-headful', vueHeadful)

router.beforeEach((to, from, next) => {
  // use the language from the routing param or default language
  let language = to.params.lang
  if (!language) {
    language = 'en'
  }
  // set the current language for i18n.
  i18n.locale = language
  next()
})

export const serverBus = new Vue()
const i = baseServ.RecreateComponent()
console.log(i)
const menu = window.localStorage.getItem('websidemenu')
if (menu === '' || menu === null) {
  window.localStorage.setItem('websidemenu', false)
}
window.localStorage.setItem('Lang', 'en')
Vue.filter('translate', function (value) {
  return i18n.t(value)
})

/* eslint-disable no-new */
new Vue({
  created () {
    AOS.init()
  },
  el: '#app',
  router,
  i18n,
  baseServ,
  render: h => h(App)
})
