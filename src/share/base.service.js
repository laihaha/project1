const previousWidth = ''
const switchMode = false
const modalOpen = false
const Device = 0
function commonFun () {

}
function CheckDeviceType () {
  const tmpDeviceType = new DeviceProperty()
  var ua = navigator.userAgent
  if (/iPhone|iPad|iPod/i.test(ua)) {
    tmpDeviceType.DeviceType = 'Mobile'
    tmpDeviceType.DevicePlatform = 'IOS'
  } else if (/Android/i.test(ua)) {
    tmpDeviceType.DeviceType = 'Mobile'
    tmpDeviceType.DevicePlatform = 'Android'
  } else if (/Macintosh/i.test(ua)) {
    tmpDeviceType.DeviceType = 'IphoneWeb'
    tmpDeviceType.DevicePlatform = 'IOSWeb'
  } else if (/MicroMessenger/i.test(ua)) {
    tmpDeviceType.DeviceType = 'Mobile'
    tmpDeviceType.DevicePlatform = 'Wechat'
  } else {
    tmpDeviceType.DeviceType = 'Web'
    tmpDeviceType.DevicePlatform = 'Browser'
  }
  return tmpDeviceType
}

function GetDeviceType () {
  const DeviceType = ''
  if (DeviceType === 'Mobile') {
    return true
  } else {
    return false
  }
}

function RecreateComponent () {
  let device = this.CheckDeviceType()
  if (window.outerWidth < 1025 && this.previousWidth > 1025) {
    this.previousWidth = window.outerWidth
    if (device.DevicePlatform === 'IOSWeb') {
      this.Device = 1
    } else {
      this.Device = 0
    }
    this.switchMode = true
  } else if (this.previousWidth < 1025 && window.outerWidth > 1025) {
    this.previousWidth = window.outerWidth
    if (device.DevicePlatform === 'IOSWeb') {
      this.Device = 1
    } else {
      this.Device = 1
    }
    this.switchMode = true
  }
}

export class DeviceProperty {
  DeviceType
  DevicePlatform
}
// 暴露出这些属性和方法
export default {
  commonFun,
  CheckDeviceType,
  GetDeviceType,
  previousWidth,
  switchMode,
  modalOpen,
  RecreateComponent,
  Device
}
