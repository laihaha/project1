import { serverBus } from '../../main.js'
export default {
  name: 'Modal',
  data () {
    return {
      isVisible: false,
      menuItems: null,
      focusedIndex: 0,
      modalOpen: false,
      inner: 0,
      isopen: 0
    }
  },
  created () {
    // Using the server bus
    serverBus.$on('serverSelected', (server) => {
      this.modalOpen = server
    })
    serverBus.$on('serverSelected1', (server1) => {
      this.isopen = server1
    })
  },
  methods: {
    close () {
      serverBus.$emit('serverSelected', !this.modalOpen)
      serverBus.$emit('serverSelected1', this.isopen = 0)
      this.inner = 0
    },
    opentomore (item) {
      this.inner = item
    },
    gotowork (item) {
      this.$router.push('/work/' + item)
      this.close()
    },
    openlink (item) {
      this.$router.push(item)
      this.close()
    }
  }
}
