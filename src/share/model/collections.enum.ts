export enum Devices {
    Mobile,
    Web,
    Wechat,
    IphoneWeb,
    Browser
}

export enum DevicesPlatform {
    IOS = "ios",
    Android = "android",
    IOSWeb = "iosWeb",
    Wechat = "wechat",
    Browser = "browser"

}


export class DeviceProperty {
    DeviceType: Devices;
    DevicePlatform: DevicesPlatform;
}

export default {
    DeviceProperty
  }