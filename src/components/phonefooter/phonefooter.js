import WebMenuBar from '../WebMenuBar/WebMenuBar'
import { serverBus } from '../../main.js'
import i18n from '../../share/i18n'
const now = new Date();
const year = now.getFullYear();
export default {
  component:{
    WebMenuBar
  },
  mounted() {
    this.menuItems = document.querySelectorAll('.MegaMenu a')
  },
  data() {
    return {
      isVisible: false,
      menuItems: null,
      focusedIndex: 0,
      modalOpen:false,
      year,
      isopen:0,
      languages: [
        { flag: 'us', language: 'en', title: 'English', img: '@/assets/img/logo/mainlogo.png' },
        { flag: 'cn', language: 'cn', title: 'Chinese', img: '@/assets/img/bgimg/china_flag.png' }        ]
    }
  },
  methods: {
    openmodal(){
      serverBus.$emit('serverSelected', !this.modalOpen)
      serverBus.$emit('serverSelected1', this.isopen=+1)
    },
    showMenu() {
      this.isVisible = true
    },
    hideMenu() {
      this.isVisible = false
      this.focusedIndex = 0
    },
    startArrowKeys() {
      this.menuItems[0].focus()
    },
    focusPrevious(isArrowKey) {
      this.focusedIndex = this.focusedIndex - 1

      if (isArrowKey) {
        this.focusItem()
      }
    },
    focusNext(isArrowKey) {
      this.focusedIndex = this.focusedIndex + 1

      if (isArrowKey) {
        this.focusItem()
      }
    },
    focusItem() {
      this.menuItems[this.focusedIndex].focus()
    },
    openlink(url) {
      this.$router.push(url)
    },
    opento(item){
      switch(item)
      {
        case 1:
            window.open("https://www.google.com/maps/search/One+Pemimpin,+1+Pemimpin+Drive,+%2309-06,+576151+Singapore/@1.3521314,103.8399571,17.5z");
        break
        case 2:
        window.location.href="mailto:Contact@techmedia.sg"
        break
        case 3:
        window.location.href="tel:+123456789"
        break
      }
    },
    changeLocale(locale) {
        i18n.locale = locale;
    }
  }
}
