const now = new Date();
const year = now.getFullYear();
import i18n from '../../share/i18n'
export default {
  component:{
  },
  data() {
    return {
      isVisible: false,
      menuItems: null,
      focusedIndex: 0,
      year,
      modalOpen:false,
      cssHome:{
        part1height:0,
      },
      window: {
        width: 0,
        height: 0
      },
      isopen:0,
      languages: [
        { flag: 'us', language: 'en', title: 'English', img: '@/assets/img/logo/mainlogo.png' },
        { flag: 'cn', language: 'cn', title: 'Chinese', img: '@/assets/img/bgimg/china_flag.png' }        ]
    }
  },
  methods: {
    scrollToTop() {
      window.scrollTo(0,0);
    },
    changeLocale(locale) {
        i18n.locale = locale;
    },
    updateScroll() {
      this.scrollPosition = window.scrollY
    },
    handleResize() {
      this.window.width = window.innerWidth;
      this.window.height = window.innerHeight;
      if(this.window.height)
      {
        this.cssHome.part1height=this.window.height*(9.5/10)
      }
    },
    openlink(url) {
      this.$router.push(url)
    },
    opento(item){
      switch(item)
      {
        case 1:
        window.open("https://www.google.com/maps/search/One+Pemimpin,+1+Pemimpin+Drive,+%2309-06,+576151+Singapore/@1.3521314,103.8399571,17.5z", "_blank");
        break
        case 2:
        window.location.href="mailto:Contact@techmedia.sg"
        break
        case 3:
        window.location.href="tel:+123456789"
        break
      }
    }
  },
  mounted() {
    window.addEventListener('scroll', this.handleScroll);
  },
  destroy() {
    window.removeEventListener('scroll', this.handleScroll)
    window.removeEventListener('resize', this.handleResize)
  },
  created() {
    window.addEventListener('resize', this.handleResize)
    window.addEventListener('scroll', this.handleScroll);
    this.handleResize();
  }
}
