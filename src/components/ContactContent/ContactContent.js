const now = new Date();
const year = now.getFullYear();
export default {
  data() {
    return {
      msg: 'Welcome to Your Vue.js App',
      year,
      selected: [],
      options: [{
        text: 'Subscribe to newsletter',
        value: 'yes'
      }],
      form: {
        email: '',
        name: '',
        food: null,
        checked: []
      },
      show: true,
      options_subject: [{
        value: 'G',
        text: 'General'
      },
      {
        value: 'O',
        text: 'Others'
      }
      ]
    }
  }
}
