const now = new Date();
const year = now.getFullYear();
export default {
  data() {
    return {
      msg: 'Welcome to Your Vue.js App',
      year,
      selected: [], // Must be an array reference!
      options: [
      {
        text: 'Mr',
        value: 'M'
      },
      {
        text: 'Ms',
        value: 'F'
      }
      ],
      items: [
      {
        text: 'HOME',
        href: '/home'
      }, {
        text: 'JOBS',
        href: ''
      }, {
        text: 'APPLY FOR A JOB',
        active: true
      }
      ],
      form: {
        email: '',
        name: '',
        food: null,
        checked: []
      },
      foods: [
      {
        text: 'Select One',
        value: null
      }, 'Carrots', 'Beans', 'Tomatoes', 'Corn'
      ],
      show: true,
      options_position: [
      {
        value: 'php',
        text: 'PHP Developer',
        selected: true
      },
      {
        value: 'design',
        text: 'Web Designer'
      }
      ],
      options_expertise: [{
        value: 'SG',
        text: 'Singapore',
        selected: true
      },
      {
        value: 'No',
        text: 'No'
      }
      ],
      options_country: [{
        value: 'MY',
        text: 'Malaysia',
        selected: true
      },
      {
        value: 'SG',
        text: 'Singapore'
      }
      ],
    }
  }
}
